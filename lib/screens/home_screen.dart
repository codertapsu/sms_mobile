import 'package:background_sms/background_sms.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:flutter/services.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  String _message = 'Waiting for message';

  Future _messageHandler(RemoteMessage message) async {
    /// something to do
    // final messageStr = message.data['message'];
    final phoneNumberStr = message.notification?.title;
    final messageStr = message.notification?.body;

    if (phoneNumberStr?.length == 10) {
      SmsStatus result = await BackgroundSms.sendMessage(
          phoneNumber: phoneNumberStr!, message: messageStr!);
      if (result == SmsStatus.sent) {
        setState(() {
          _message = 'Sent to $phoneNumberStr';
        });
      } else {
        setState(() {
          _message = 'Failed';
        });
      }
    }
  }

  Future<void> _showMyDialog(String token) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('AlertDialog Title'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                const Text('Your FCM token'),
                Text(token),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.pop(context, 'Close'),
              child: const Text('Close'),
            ),
            TextButton(
              onPressed: () {
                Clipboard.setData(ClipboardData(text: token)).then((_) {
                  const snackBar = SnackBar(
                    content: Text('Copied to clipboard'),
                  );
                  ScaffoldMessenger.of(context).showSnackBar(snackBar);
                });
                Navigator.pop(context, 'Copy');
              },
              child: const Text('Copy'),
            ),
          ],
        );
      },
    );
  }

  @override
  void initState() {
    super.initState();
    //Handle messages in background
    // FirebaseMessaging.onBackgroundMessage(messageHandler);
    // Handle messages in foreground
    Permission.sms.request().isGranted.then((permissionStatus) => {
          FirebaseMessaging.onMessage
              .listen((message) => _messageHandler(message))
        });
    FirebaseMessaging.instance.getToken().then((token) {
      print('token: $token');
      _showMyDialog(token!);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('SMS-MAN'),
      ),
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              _message,
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
    );
  }
}
